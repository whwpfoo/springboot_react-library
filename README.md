# 도서 관리/대여 서비스 v1.0

- 소유하고 있는 책을 관리하고 싶어서 만들기 시작  
- SPA 구성을 위해 Vue.js 적용
- http://www.whwp-library.cf

## 개발 환경

- backend: Java, Spring Boot, Spring Data JPA
- frontend: ES6, Vue.js (axios, router...)
- build: gradle
- server: nginx
- database: MariaDB 

## 기능

- `ISBN` 코드를 이용한 간편 도서 등록/관리
- 회원 간 도서 대여 및 추적 관리

## 추가기능

- 소셜 로그인
- ISBN 바코드 앱을 통한 도서 등록 기능