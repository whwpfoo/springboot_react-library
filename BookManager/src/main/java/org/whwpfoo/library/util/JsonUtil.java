package org.whwpfoo.library.util;

import com.google.gson.Gson;

import java.util.Map;

public class JsonUtil {

    public static <T> T toObject(String json, Class<T> type) {
        Gson gson = new Gson();
        return gson.fromJson(json, type);
    }

    public static <T> T toObject(Map map, Class<T> type) {
        return toObject(toString(map), type);
    }

    public static Map<String, Object> toMap(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Map.class);
    }

    public static String toString(Map<String, Object> map) {
        Gson gson = new Gson();
        return gson.toJson(map);
    }


}
