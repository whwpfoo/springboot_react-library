package org.whwpfoo.library.util;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public class HttpClientUtil {

    public static String sendGet(String url, Map<String, String> headerMap, Map<String, String> paramMap) {
        String result = null;

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            StringBuilder sb = new StringBuilder();
            sb.append(url);

            Optional.ofNullable(paramMap).ifPresent(map -> { // queryString 설정
                sb.append("?");
                map.entrySet().forEach(param -> {
                        sb.append(param.getKey()).append("=").append(param.getValue());
                        if (map.size() > 1) sb.append("&");
                });
            });

            HttpGet httpGet = new HttpGet(sb.toString());

            Optional.ofNullable(headerMap).ifPresent(map -> { // header 설정
                map.entrySet().forEach(header -> {
                    httpGet.addHeader(header.getKey(), header.getValue());
                });
            });

            try (CloseableHttpResponse httpResponse = httpClient.execute(httpGet)) {
                result = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            }

        } catch (IOException io) {
        }

        return result;
    }

    public static String sendGet(String url, Map<String, String> paramMap) {
        return sendGet(url, null, paramMap);
    }

    public static String sendGet(String url) {
        return sendGet(url, null, null);
    }

}
