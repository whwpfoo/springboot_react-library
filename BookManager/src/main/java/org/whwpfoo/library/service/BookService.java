package org.whwpfoo.library.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.domain.books.BookRepository;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.domain.rentals.RentalRepository;
import org.whwpfoo.library.exception.BookException;
import org.whwpfoo.library.exception.CommonException;
import org.whwpfoo.library.web.dto.books.BookSaveRequestDto;
import org.whwpfoo.library.web.dto.books.BookViewResponseDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

import static org.whwpfoo.library.exception.BookException.NOT_FOUND_BOOK;
import static org.whwpfoo.library.exception.BookException.WRONG_FILE_TYPE;
import static org.whwpfoo.library.exception.CommonException.CHECKED_ERROR;

@Service
@RequiredArgsConstructor
@Transactional
public class BookService {
    private final Logger log = LoggerFactory.getLogger(BookService.class);
    private final NaverApiService naverApiService;
    private final BookRepository bookRepository;
    private final RentalRepository rentalRepository;

    public long saveBook(BookSaveRequestDto saveRequestDto) {
        String isbnCode = saveRequestDto.getIsbn();

        if (StringUtils.isNotBlank(isbnCode)) {
            String bookInfoToJson = naverApiService.searchBookByIsbnCode(isbnCode);
            saveRequestDto = BookSaveRequestDto.fromJson(bookInfoToJson);

            Optional.ofNullable(saveRequestDto).orElseThrow(() -> {
                log.error("fail to find a book [isbnCode={}]", isbnCode);
                return new BookException(NOT_FOUND_BOOK);
            });

            saveRequestDto.setIsbn(isbnCode);
        }

        return bookRepository.save(saveRequestDto.toEntity()).getId();
    }

    public String saveBookByCsvFile(MultipartFile csvFile) {
        String originalFilename = Optional.ofNullable(csvFile)
                .map(MultipartFile::getOriginalFilename).orElseThrow(() -> new BookException(WRONG_FILE_TYPE));

        StringBuilder isbnCode = new StringBuilder();

        try (BufferedReader bfr = new BufferedReader(new InputStreamReader(csvFile.getInputStream()))) {
            String ext = originalFilename.split("\\.")[1];

            if (!"csv".equalsIgnoreCase(ext)) {
                throw new RuntimeException();
            }

            for (String line; (line = bfr.readLine()) != null; ) {
                isbnCode.append(line);
            }
        } catch (RuntimeException e) {
            throw new CommonException(WRONG_FILE_TYPE);
        } catch (IOException e) {
            log.error("file IO error [msg={}]", e.getMessage());
            throw new CommonException(CHECKED_ERROR);
        }

        return saveBookByIsbnCodes(isbnCode.toString().split(","));
    }

    public String saveBookByIsbnCodes(String[] isbnCodes) {
        int success = 0;
        int fail = 0;
        StringBuilder failIsbnCode = new StringBuilder();

        for (String isbnCode : isbnCodes) {
            try {
                String bookInfoToJson = naverApiService.searchBookByIsbnCode(isbnCode);
                BookSaveRequestDto saveRequestDto = BookSaveRequestDto.fromJson(bookInfoToJson);
                saveRequestDto.setIsbn(isbnCode);
                bookRepository.save(saveRequestDto.toEntity());
                success++;
            } catch (Exception e) {
                failIsbnCode.append(isbnCode).append(" ");
                fail++;
            }
        }

        return "등록: " + success + "건\n실패: " + fail + "건\n실패 목록: " + failIsbnCode.toString();
    }

    public BookViewResponseDto findBook(Long bookId) {
        Books findBook = bookRepository.findById(bookId).orElseThrow(() -> new BookException(NOT_FOUND_BOOK));
        return new BookViewResponseDto(findBook);
    }

    public Page<BookViewResponseDto> findBookList(String title, BookStatus status, Pageable pageable) {
        return searchBook(title, status, pageable);
    }

    public Page<BookViewResponseDto> searchBook(String title, BookStatus status, Pageable pageable) {
        log.info("search book -> [title={}\nstatus={}]", title, status);
        if (StringUtils.isNotBlank(title) || status != null) {
            if (status == null) { // 제목 검색
                return bookRepository.findByTitleContaining(title, pageable).map(BookViewResponseDto::new);
            } else if (StringUtils.isBlank(title)) { // 상태 검색
                return bookRepository.findByBookStatus(status, pageable).map(BookViewResponseDto::new);
            } else { // 제목, 상태 검색
                return bookRepository.findByTitleContainingAndBookStatus(title, status, pageable).map(BookViewResponseDto::new);
            }
        }

        return bookRepository.findAll(pageable).map(BookViewResponseDto::new);
    }

    public void deleteBook(Long bookId) {
        try {
            log.info("delete book -> [bookId={}]", bookId);
            rentalRepository.deleteByBooks(Books.builder().id(bookId).build());
            bookRepository.deleteById(bookId);
        } catch (Exception e) {
            log.error("delete book error [id={}\nmsg={}]", bookId, e.getMessage());
            throw new CommonException(CHECKED_ERROR);
        }
    }
}
