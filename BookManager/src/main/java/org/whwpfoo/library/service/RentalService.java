package org.whwpfoo.library.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.domain.books.BookRepository;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.domain.rentals.RentalRepository;
import org.whwpfoo.library.domain.rentals.Rentals;
import org.whwpfoo.library.domain.users.Users;
import org.whwpfoo.library.exception.BookException;
import org.whwpfoo.library.exception.RentalException;
import org.whwpfoo.library.web.dto.rentals.RentalListResponseDto;
import org.whwpfoo.library.web.dto.rentals.RentalRequestDto;
import org.whwpfoo.library.web.dto.rentals.RentalResponseDto;

import static org.whwpfoo.library.exception.BookException.ALREADY_RENTAL_BOOK;
import static org.whwpfoo.library.exception.BookException.NOT_FOUND_BOOK;
import static org.whwpfoo.library.exception.RentalException.NOT_FOUND_RENTAL_INFO;


@Service
@RequiredArgsConstructor
@Transactional
public class RentalService {
    private final Logger log = LoggerFactory.getLogger(RentalService.class);
    private final RentalRepository rentalRepository;
    private final BookRepository bookRepository;

    public long requestRentalBook(Long userId, Long bookId) {
        Books bookEntity = bookRepository.findById(bookId).orElseThrow(() -> new BookException(NOT_FOUND_BOOK));

        if (bookEntity.getBookStatus() == BookStatus.OFF) {
            log.info("already rentaled book -> {}", bookId);
            throw new BookException(ALREADY_RENTAL_BOOK);
        }

        Rentals rentalEntity = rentalRepository.save(
                RentalRequestDto.builder()
                .user_id(userId)
                .book_id(bookId)
                .build().toEntity());

        bookEntity.changeBookStatus(BookStatus.OFF);
        return rentalEntity.getId();
    }

    public RentalResponseDto performRentalBook(Long rentalId, RentalStatus rentalStatus) {
        Rentals rentalEntity = null;

        if (RentalStatus.ACCEPT == rentalStatus) {
            rentalEntity = allowRentalBook(rentalId);
        } else {

            if (RentalStatus.REJECT == rentalStatus) {
                rentalEntity = rejectRentalBook(rentalId);
            } else {
                rentalEntity = returnRentalBook(rentalId);
            }
            rentalEntity.getBooks().changeBookStatus(BookStatus.ON); // 도서 상태 업데이트
        }

        return new RentalResponseDto(rentalEntity);
    }

    private Rentals allowRentalBook(Long rentalId) { // 대여 승인
        return updateRentalStatus(rentalId, RentalStatus.ACCEPT);
    }

    private Rentals rejectRentalBook(Long rentalId) { // 대여 거절
        return updateRentalStatus(rentalId, RentalStatus.REJECT);
    }

    private Rentals returnRentalBook(Long rentalId) { // 대여 반납
        return updateRentalStatus(rentalId, RentalStatus.RETURN);
    }

    private Rentals updateRentalStatus(Long rentalId, RentalStatus rentalStatus) {
        return rentalRepository.findById(rentalId)
                .map(rental -> rental.updateRentalStatus(rentalStatus))
                .orElseThrow(() -> new RentalException(NOT_FOUND_RENTAL_INFO));
    }

    public Page<RentalListResponseDto> findRentalList(RentalStatus rentalStatus, Long userId, int auth, String search, Pageable pageable) {
        return searchRental(rentalStatus, userId, auth, search, pageable);
    }

    private Page<RentalListResponseDto> searchRental(RentalStatus rentalStatus, Long userId, int auth, String search, Pageable pageable) {
        if (UserType.USER.getAuth() == auth) {
            return searchRentalByUser(rentalStatus, userId, pageable);
        }
        return searchRentalByAdmin(rentalStatus, search, pageable);
    }

    private Page<RentalListResponseDto> searchRentalByAdmin(RentalStatus rentalStatus, String search, Pageable pageable) {
        search = StringUtils.isBlank(search) ? null : search;
        return rentalRepository.findByRentalListForAdmin(rentalStatus, search, pageable).map(RentalListResponseDto::new);
    }

    private Page<RentalListResponseDto> searchRentalByUser(RentalStatus rentalStatus, Long userId, Pageable pageable) {
        Users userEntity = Users.builder().id(userId).build();
        return rentalRepository.findByRentalListForUser(userEntity, rentalStatus, pageable).map(RentalListResponseDto::new);
    }
}