package org.whwpfoo.library.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.whwpfoo.library.config.NaverApiConfig;
import org.whwpfoo.library.exception.BookException;
import org.whwpfoo.library.util.HttpClientUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static org.whwpfoo.library.exception.BookException.INCORRECT_ISBN_CODE;

@Service
@RequiredArgsConstructor
public class NaverApiService {

    private final NaverApiConfig naverApiConfig;

    public String searchBookByIsbnCode(String isbnCode) {
        if (!validateIsbnCode(isbnCode)) {
            throw new BookException(INCORRECT_ISBN_CODE);
        }

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("X-Naver-Client-Id", naverApiConfig.getId());
        headerMap.put("X-Naver-Client-Secret", naverApiConfig.getSecret());

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("query", isbnCode);

        return HttpClientUtil.sendGet(naverApiConfig.getUrl(), headerMap, paramMap);
    }

    private boolean validateIsbnCode(String isbnCode) {
        return Pattern.matches("^[0-9\\-]*$", isbnCode);
    }

}
