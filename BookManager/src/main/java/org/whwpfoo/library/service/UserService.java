package org.whwpfoo.library.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.domain.users.UserRepository;
import org.whwpfoo.library.exception.UserException;
import org.whwpfoo.library.util.EncryptHelper;
import org.whwpfoo.library.web.dto.users.UserSaveRequestDto;
import org.whwpfoo.library.web.dto.users.UserUpdateRequestDto;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;

import static org.whwpfoo.library.exception.UserException.*;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService {
    private final Logger log = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final EncryptHelper encryptHelper;

    public long saveUser(UserSaveRequestDto userSaveRequestDto) {
        String email = userSaveRequestDto.getEmail();

        if (userRepository.existsByEmail(email)) { // 중복 아이디 검사
            log.info("saveUser fail ! exist email -> [email={}]", email);
            throw new UserException(EXIST_EMAIL);
        }

        userSaveRequestDto.setPassword(encryptHelper.encrypt(userSaveRequestDto.getPassword()));
        return userRepository.save(userSaveRequestDto.toEntity()).getId();
    }

    public UserViewResponseDto updateUserInfo(UserUpdateRequestDto userUpdateRequestDto) { // session update 필요
        return userRepository.findById(userUpdateRequestDto.getId())
                .map(user -> user.updateUserInfo(userUpdateRequestDto))
                .map(UserViewResponseDto::new)
                .orElseThrow(() -> new UserException(NOT_FOUND_USER));
    }

    public UserViewResponseDto changeUserType(long id, UserType userType) { // session update 필요
        return userRepository.findById(id)
                .map(user -> user.changeUserType(userType))
                .map(UserViewResponseDto::new)
                .orElseThrow(() -> new UserException(NOT_FOUND_USER));
    }

    public void changePassword(UserUpdateRequestDto userUpdateRequestDto) {
        userRepository.findById(userUpdateRequestDto.getId())
                .filter(user -> isCorrectPassword(userUpdateRequestDto.getOldPassword(), user.getPassword()))
                .map(user -> user.changePassword(encryptHelper.encrypt(userUpdateRequestDto.getPassword())))
                .orElseThrow(() -> new UserException(NOT_MATCH_PASSWORD));
    }

    public Page<UserViewResponseDto> findUserList(Pageable pageable) {
        return userRepository.findAll(pageable).map(UserViewResponseDto::new);
    }

    public UserViewResponseDto login(String email, String password) {
        return userRepository.findByEmail(email)
                .filter(user -> isCorrectPassword(password, user.getPassword()))
                .map(UserViewResponseDto::new)
                .orElseThrow(() -> new UserException(WRONG_EMAIL_OR_PASSWORD));
    }

    private boolean isCorrectPassword(String currentPassword, String hashedPassword) {
        return encryptHelper.isMatch(currentPassword, hashedPassword);
    }
}
