package org.whwpfoo.library.domain.users;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.domain.DefaultTimeEntity;
import org.whwpfoo.library.domain.rentals.Rentals;
import org.whwpfoo.library.web.dto.users.UserUpdateRequestDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
public class Users extends DefaultTimeEntity {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false) // USER, ADMIN
    @Enumerated(EnumType.STRING)
    private UserType userType;

    @Builder
    public Users(Long id, String email, String password, String username, UserType userType) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.username = username;
        this.userType = userType;
    }

    public Users updateUserInfo(UserUpdateRequestDto userUpdateRequestDto) {
        this.username = userUpdateRequestDto.getUsername();
        return this;
    }

    public Users changePassword(String newPassword) {
        this.password = newPassword;
        return this;
    }

    public Users changeUserType(UserType userType) {
        this.userType = userType;
        return this;
    }
}
