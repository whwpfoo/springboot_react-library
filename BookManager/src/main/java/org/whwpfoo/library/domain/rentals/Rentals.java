package org.whwpfoo.library.domain.rentals;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.domain.DefaultTimeEntity;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.domain.users.Users;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
public class Rentals extends DefaultTimeEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rental_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Users users;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    private Books books;

    @Column(length = 500)
    private String message;

    @Column
    @Enumerated(EnumType.STRING)
    private RentalStatus rentalStatus;

    private LocalDateTime returnDate;

    @Builder
    public Rentals(Books books, Users users, String message, RentalStatus rentalStatus, LocalDateTime returnDate) {
        this.books = books;
        this.users = users;
        this.message = message;
        this.rentalStatus = rentalStatus;
        this.returnDate = returnDate;
    }

    public Rentals updateRentalStatus(RentalStatus rentalStatus) {
        this.rentalStatus = rentalStatus;

        switch (this.rentalStatus) {
            case ACCEPT:
                this.message = "대여 중";
                break;
            case REJECT:
                this.message = "대여 거절";
                break;
            case RETURN:
                this.message = "도서 반납";
                this.returnDate = LocalDateTime.now();
                break;
        }

        return this;
    }
}
