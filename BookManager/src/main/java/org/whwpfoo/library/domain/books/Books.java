package org.whwpfoo.library.domain.books;

import lombok.*;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.domain.DefaultTimeEntity;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
public class Books extends DefaultTimeEntity {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Long id;

    private String isbn;

    @Column(nullable = false, length = 500)
    private String title;

    @Column(nullable = false)
    private String author;

    private String price;

    private String discount;

    private String publisher;

    @Column(length = 2000)
    private String description;

    @Column(length = 1000)
    private String image;

    private String pubdate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private BookStatus bookStatus;

    @Builder
    public Books(Long id, String isbn, String title, String author, String price, String discount,
                 String publisher, String description, String image, String pubdate, BookStatus bookStatus) {
        this.id = id;
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.price = price;
        this.discount = discount;
        this.publisher = publisher;
        this.description = description;
        this.image = image;
        this.pubdate = pubdate;
        this.bookStatus = bookStatus;
    }

    public void changeBookStatus(BookStatus bookStatus) {
        this.bookStatus = bookStatus;
    }
}
