package org.whwpfoo.library.domain.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.whwpfoo.library.domain.rentals.Rentals;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Long> {

    boolean existsByEmail(String email);
    Optional<Users> findByEmail(String email);
}
