package org.whwpfoo.library.domain;

import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

@NoArgsConstructor
public final class PageRequest {
    private int page;
    private int size;
    private Sort.Direction sort;

    public void setPage(int page) {
        this.page = page <= 0 ? 1 : page;
    }

    public void setSize(int size) {
        int DEFAULT_SIZE = 20;
        int MAX_SIZE = 1000;
        this.size = size > MAX_SIZE ? DEFAULT_SIZE : size;
    }

    public void setSort(Sort.Direction sort) {
        this.sort = sort == null ? Sort.Direction.DESC : sort;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public Sort.Direction getSort() {
        return sort;
    }

    public org.springframework.data.domain.PageRequest of() {
        return org.springframework.data.domain.PageRequest.of(page - 1, size, sort, "createdDate");
    }
}
