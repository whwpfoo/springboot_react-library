package org.whwpfoo.library.domain.books;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.whwpfoo.library.code.BookStatus;

public interface BookRepository extends JpaRepository<Books, Long> {
    Page<Books> findByBookStatus(BookStatus status, Pageable pageable);
    Page<Books> findByTitleContaining(String title, Pageable pageable);
    Page<Books> findByTitleContainingAndBookStatus(String title, BookStatus status, Pageable pageable);
}
