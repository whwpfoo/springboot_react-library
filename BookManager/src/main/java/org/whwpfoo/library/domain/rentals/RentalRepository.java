package org.whwpfoo.library.domain.rentals;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.domain.users.Users;

public interface RentalRepository extends JpaRepository<Rentals, Long> {
    // 관리자 대여 조회 쿼리
    @Query(value =
            "select r from Rentals as r "
                + "left join fetch r.users u "
                + "left join fetch r.books b "
                + "where (:status is null or r.rentalStatus = :status) "
                + "and (:search is null or r.users.username = :search) ",
            countQuery =
            "select count(r) from Rentals r "
                + "left join r.users u "
                + "left join r.books b "
                + "where (:status is null or r.rentalStatus = :status) "
                + "and (:search is null or u.username = :search) "
    )
    Page<Rentals> findByRentalListForAdmin(@Param("status") RentalStatus rentalStatus,
                                           @Param("search") String search,
                                           Pageable pageable);

    // 사용자 대여 조회 쿼리
    @Query(value =
            "select r from Rentals as r "
                    + "left join fetch r.users u "
                    + "left join fetch r.books b "
                    + "where r.users = :users "
                    + "and (:status is null or r.rentalStatus = :status)",
            countQuery =
                    "select count(r) from Rentals r "
                            + "left join r.users u "
                            + "left join r.books b "
                            + "where r.users = :users "
                            + "and (:status is null or r.rentalStatus = :status)"
    )
    Page<Rentals> findByRentalListForUser(@Param("users") Users users,
                                          @Param("status") RentalStatus rentalStatus,
                                          Pageable pageable);

    void deleteByBooks(Books books);
}
