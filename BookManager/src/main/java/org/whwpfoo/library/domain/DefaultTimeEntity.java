package org.whwpfoo.library.domain;

import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Getter
@MappedSuperclass //JPA Entity 클래스들이 해당 클래스를 상속할 경우 해당 클래스의 필드들도 컬럼으로 인식
@EntityListeners(AuditingEntityListener.class) // JPA Auditing 기능을 사용하는 설정
public abstract class DefaultTimeEntity {

    @CreatedDate // Entity 가 생성되어 저장될 때  저장시간 자동저장
    private LocalDateTime createdDate;

    @LastModifiedDate // 조회한 Entity의 값이 변경되는 경우 변경시간 자동저장
    private LocalDateTime modifiedDate;
}
