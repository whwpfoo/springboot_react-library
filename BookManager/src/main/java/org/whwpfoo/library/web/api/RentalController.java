package org.whwpfoo.library.web.api;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.whwpfoo.library.code.LoginType;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.domain.PageRequest;
import org.whwpfoo.library.service.RentalService;
import org.whwpfoo.library.web.dto.rentals.RentalListResponseDto;
import org.whwpfoo.library.web.dto.rentals.RentalResponseDto;
import org.whwpfoo.library.web.mvc.NeedLogin;
import org.whwpfoo.library.web.mvc.UserSessionHolder;

@RestController
@RequiredArgsConstructor
public class RentalController {
    private final Logger log = LoggerFactory.getLogger(RentalController.class);
    private final RentalService rentalService;
    private final UserSessionHolder userSessionHolderImpl;

    @PostMapping(value = "/api/rentals/book-request", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.USER)
    public ResponseEntity<Long> requestRental(@RequestParam(name = "book_id", value = "book_id") Long bookId) {
        long userId = userSessionHolderImpl.payload().getId();
        log.info("rental book id -> {}", bookId);
        log.info("rental user id -> {}", userId);
        long rentalId = rentalService.requestRentalBook(userId, bookId);

        return new ResponseEntity<>(rentalId, HttpStatus.OK);
    }

    @PostMapping(value = "/api/rentals/book-response", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.ADMIN)
    public ResponseEntity<RentalResponseDto> performRental(@RequestParam(value = "rentalId", required = true) Long rentalId,
                                                           @RequestParam(value = "status", required = true) RentalStatus status) {
        return new ResponseEntity<>(rentalService.performRentalBook(rentalId, status), HttpStatus.OK);
    }

    @GetMapping(value = "/api/rentals/books", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.ALL)
    public ResponseEntity<Page<RentalListResponseDto>> listRental(
            @RequestParam(value = "status", required = false) RentalStatus rentalStatus,
            @RequestParam(value = "search", required = false) String search,
            final PageRequest pageable) {
        long userId = userSessionHolderImpl.payload().getId();
        int auth = userSessionHolderImpl.payload().getAuth();
        Page<RentalListResponseDto> rentalList = rentalService.findRentalList(rentalStatus, userId, auth, search, pageable.of());

        return new ResponseEntity<>(rentalList, HttpStatus.OK);
    }
}
