package org.whwpfoo.library.web.dto.rentals;

import lombok.Getter;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.domain.rentals.Rentals;

import java.time.LocalDateTime;

@Getter
public class RentalResponseDto {

    private Long id;
    private LocalDateTime returnDate;
    private RentalStatus rentalStatus;
    private String message;

    public RentalResponseDto(Rentals rentals) {
        this.id = rentals.getId();
        this.returnDate = rentals.getReturnDate();
        this.rentalStatus = rentals.getRentalStatus();
        this.message = rentals.getMessage();
    }
}
