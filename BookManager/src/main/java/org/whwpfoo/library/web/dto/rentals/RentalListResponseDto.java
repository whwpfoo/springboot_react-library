package org.whwpfoo.library.web.dto.rentals;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.domain.rentals.Rentals;
import org.whwpfoo.library.web.dto.books.BookViewResponseDto;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
public class RentalListResponseDto {
    private Long id;
    private BookViewResponseDto bookViewResponseDto;
    private UserViewResponseDto userViewResponseDto;
    private LocalDateTime requestDate;
    private LocalDateTime returnDate;
    private RentalStatus rentalStatus;
    private String message;

    public RentalListResponseDto(Rentals rentals) {
        this.id = rentals.getId();
        this.bookViewResponseDto = new BookViewResponseDto(rentals.getBooks());
        this.userViewResponseDto = new UserViewResponseDto(rentals.getUsers());
        this.requestDate = rentals.getCreatedDate();
        this.returnDate = rentals.getReturnDate();
        this.rentalStatus = rentals.getRentalStatus();
        this.message = rentals.getMessage();
    }
}
