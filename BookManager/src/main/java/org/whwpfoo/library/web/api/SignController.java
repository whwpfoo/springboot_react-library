package org.whwpfoo.library.web.api;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.whwpfoo.library.code.LoginType;
import org.whwpfoo.library.service.UserService;
import org.whwpfoo.library.web.dto.users.SignRequestDto;
import org.whwpfoo.library.web.dto.users.UserSaveRequestDto;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;
import org.whwpfoo.library.web.mvc.NeedLogin;
import org.whwpfoo.library.web.mvc.UserSessionHolder;

@RestController
@RequiredArgsConstructor
public class SignController {
    private final Logger log = LoggerFactory.getLogger(SignController.class);
    private final UserService userService;
    private final UserSessionHolder userSessionHolderImpl;

    @PostMapping(value = "/api/users/login", produces = "application/json; charset=UTF-8")
    public ResponseEntity<UserViewResponseDto> login(@RequestBody SignRequestDto signRequestDto) {
        UserViewResponseDto userDto = userService.login(signRequestDto.getEmail(), signRequestDto.getPassword());
        userSessionHolderImpl.login(userDto); // session 에 dto 세팅
        log.info("login user -> [email={}]", userDto.getEmail());
        return new ResponseEntity<>(userSessionHolderImpl.payload(), HttpStatus.OK);
    }

    @PostMapping(value = "/api/users/join", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Long> join(@RequestBody UserSaveRequestDto userSaveRequestDto) {
        long userId = userService.saveUser(userSaveRequestDto);
        log.info("join user -> [email={}]", userSaveRequestDto.getEmail());
        return new ResponseEntity<>(userId, HttpStatus.OK);
    }

    @GetMapping(value = "/api/users/logout", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.ALL)
    public ResponseEntity<String> logout() {
        if (userSessionHolderImpl.isLogin()) { // session 에 dto 제거
            log.info("logout user -> [email={}]", userSessionHolderImpl.logout().getEmail());
        }
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping(value = "/api/users/session")
    public ResponseEntity<UserViewResponseDto> getSession() {
        return new ResponseEntity<>(userSessionHolderImpl.payload(), HttpStatus.OK);
    }
}