package org.whwpfoo.library.web.dto.users;

import lombok.*;

@NoArgsConstructor
@Getter
@ToString
public class UserUpdateRequestDto {

    private Long id;
    private String username;
    private String oldPassword;
    @Setter private String password;

    @Builder
    public UserUpdateRequestDto(Long id, String username, String oldPassword, String password) {
        this.id = id;
        this.username = username;
        this.oldPassword = oldPassword;
        this.password = password;
    }

}
