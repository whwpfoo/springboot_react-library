package org.whwpfoo.library.web.api;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.whwpfoo.library.code.LoginType;
import org.whwpfoo.library.service.UserService;
import org.whwpfoo.library.web.dto.users.UserUpdateRequestDto;
import org.whwpfoo.library.web.mvc.NeedLogin;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final Logger log = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    @PutMapping(value = "/api/users/update", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.ALL)
    public ResponseEntity<String> updateUserInfo(@RequestBody UserUpdateRequestDto userUpdateRequestDto) {
        log.info("password update -> [user={}]", userUpdateRequestDto.getId());
        userService.changePassword(userUpdateRequestDto);
        return new ResponseEntity<>("변경되었습니다", HttpStatus.OK);
    }
}
