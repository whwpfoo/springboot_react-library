package org.whwpfoo.library.web;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.whwpfoo.library.exception.BookException;
import org.whwpfoo.library.exception.CommonException;
import org.whwpfoo.library.exception.RentalException;
import org.whwpfoo.library.exception.UserException;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
@RequiredArgsConstructor
public class ControllerExceptionHandler {
    private final Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);
    private final HttpHeaders headers = new HttpHeaders();

    {
//        headers.add("Content-Type", "text/plain; charset=UTF-8");
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
    }

    @ExceptionHandler(CommonException.class)
    public ResponseEntity<String> handleCommonException(HttpServletResponse response, CommonException e) {
        HttpStatus httpStatus = null;
        int errorCode = e.getCode();

        if (CommonException.CHECKED_ERROR == errorCode) httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        if (CommonException.WRONG_FILE_TYPE == errorCode) httpStatus = HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>(e.getMessage(), headers, httpStatus);
    }

    @ExceptionHandler(BookException.class)
    public ResponseEntity<String> handleBookException(HttpServletResponse response, BookException e) {
        HttpStatus httpStatus = null;
        int errorCode = e.getCode();

        if (BookException.NOT_FOUND_BOOK == errorCode) httpStatus = HttpStatus.BAD_REQUEST;
        if (BookException.INCORRECT_ISBN_CODE == errorCode) httpStatus = HttpStatus.BAD_REQUEST;
        if (BookException.ALREADY_RENTAL_BOOK == errorCode) httpStatus = HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>(e.getMessage(), headers, httpStatus);
    }

    @ExceptionHandler(UserException.class)
    public ResponseEntity handleUserException(HttpServletResponse response, UserException e) {
        HttpStatus httpStatus = null;
        int errorCode = e.getCode();

        if (UserException.EXIST_EMAIL == errorCode) httpStatus = HttpStatus.BAD_REQUEST;
        if (UserException.WRONG_EMAIL_OR_PASSWORD == errorCode) httpStatus = HttpStatus.BAD_REQUEST;
        if (UserException.NOT_FOUND_USER == errorCode) httpStatus = HttpStatus.NOT_FOUND;
        if (UserException.NOT_MATCH_PASSWORD == errorCode) httpStatus = HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>(e.getMessage(), headers, httpStatus);
    }

    @ExceptionHandler(RentalException.class)
    public ResponseEntity handleRentalException(HttpServletResponse response, RentalException e) {
        HttpStatus httpStatus = null;
        int errorCode = e.getCode();

        if (RentalException.NOT_FOUND_RENTAL_INFO == errorCode) httpStatus = HttpStatus.NOT_FOUND;

        return new ResponseEntity<>(e.getMessage(), headers, httpStatus);
    }
}
