package org.whwpfoo.library.web.dto.rentals;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.domain.rentals.Rentals;
import org.whwpfoo.library.domain.users.Users;

@NoArgsConstructor
@Getter
@ToString
public class RentalRequestDto {

    private Long book_id;
    private Long user_id;

    @Builder
    public RentalRequestDto(Long book_id, Long user_id) {
        this.book_id = book_id;
        this.user_id = user_id;
    }

    public Rentals toEntity() {
        return Rentals.builder()
                .books(Books.builder().id(book_id).build())
                .users(Users.builder().id(user_id).build())
                .rentalStatus(RentalStatus.WAITING)
                .message("대여 신청")
                .build();
    }
}
