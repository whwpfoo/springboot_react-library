package org.whwpfoo.library.web.dto.books;

import lombok.*;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.util.JsonUtil;

import java.util.List;
import java.util.Map;

@NoArgsConstructor
@Getter
@ToString
public class BookSaveRequestDto {

    @Setter private String isbn;
    private String title;
    private String author;
    private String price;
    private String discount;
    private String publisher;
    private String description;
    private String image;
    private String pubdate;
    private BookStatus bookStatus = BookStatus.ON;

    @Builder
    public BookSaveRequestDto(String isbn, String title, String author, String price, String discount, String publisher,
                              String description, String image, String pubdate) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.price = price;
        this.discount = discount;
        this.publisher = publisher;
        this.description = description;
        this.image = image;
        this.pubdate = pubdate;
    }

    public Books toEntity() {
        return Books.builder()
                .isbn(isbn)
                .title(title)
                .author(author)
                .price(price)
                .discount(discount)
                .publisher(publisher)
                .description(description)
                .image(image)
                .pubdate(pubdate)
                .bookStatus(bookStatus)
                .build();
    }

    public static BookSaveRequestDto fromJson(String json) {
        try {
            Map<String, Object> result = JsonUtil.toMap(json);
            Map<String, Object> book = ((List<Map>)result.get("items")).get(0);
            return JsonUtil.toObject(book, BookSaveRequestDto.class);
        } catch (Exception e) {
        }
        return null;
    }
}
