package org.whwpfoo.library.web.dto.users;

import lombok.*;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.domain.users.Users;

@NoArgsConstructor
@Getter
@ToString
public class UserSaveRequestDto {

    private String email;
    private String username;
    @Setter private String password;
    private UserType userType = UserType.USER;

    @Builder
    public UserSaveRequestDto(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public Users toEntity() {
        return Users.builder()
                .email(email)
                .username(username)
                .password(password)
                .userType(userType)
                .build();
    }

}
