package org.whwpfoo.library.web.dto.users;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class SignRequestDto {
    private String email;
    private String password;

    @Builder
    public SignRequestDto(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
