package org.whwpfoo.library.web;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class IndexController implements ErrorController {

    @GetMapping("/")
    public String welcome() {
        return "index";
    }

    @GetMapping("/error")
    public String error() {
        return "index";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
