package org.whwpfoo.library.web.dto.books;

import lombok.Getter;
import lombok.ToString;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.domain.books.Books;

@Getter
@ToString
public class BookViewResponseDto {

    private Long id;
    private String isbn;
    private String title;
    private String author;
    private String price;
    private String discount;
    private String publisher;
    private String description;
    private String image;
    private String pubdate;
    private BookStatus bookStatus;

    public BookViewResponseDto(Books books) {
        this.id = books.getId();
        this.isbn = books.getIsbn();
        this.title = books.getTitle();
        this.author = books.getAuthor();
        this.price = books.getPrice();
        this.discount = books.getDiscount();
        this.publisher = books.getPublisher();
        this.description = books.getDescription();
        this.image = books.getImage();
        this.pubdate = books.getPubdate();
        this.bookStatus = books.getBookStatus();
    }
}
