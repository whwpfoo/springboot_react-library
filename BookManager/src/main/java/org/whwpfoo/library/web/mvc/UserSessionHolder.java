package org.whwpfoo.library.web.mvc;

import org.whwpfoo.library.web.dto.users.UserViewResponseDto;

public interface UserSessionHolder {

    boolean isLogin();

    void login(UserViewResponseDto user);

    UserViewResponseDto logout();

    UserViewResponseDto payload();

}
