package org.whwpfoo.library.web.mvc;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.code.LoginType;
import org.whwpfoo.library.common.ResponseData;
import org.whwpfoo.library.exception.CommonException;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginCheckInterceptor extends HandlerInterceptorAdapter {

    private static Logger log = LoggerFactory.getLogger(LoginCheckInterceptor.class);

    @Autowired
    private UserSessionHolder userSessionHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.info("LoginCheckInterceptor is call! [handle = {}]", handler);

        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod hm = (HandlerMethod) handler; // 접근 컨트롤러 메소드

        // 권한 체크 시작
        NeedLogin needLogin = hm.getMethod().getAnnotation(NeedLogin.class); // 메소드의 정보 중 어노테이션 정보 추출
        if (null == needLogin) {
            needLogin = hm.getBeanType().getAnnotation(NeedLogin.class);
        }
        if (needLogin != null) {
            LoginType type = needLogin.value(); // USER, ADMIN, ALL

            if (userSessionHolder.isLogin()) {
                UserViewResponseDto userViewResponseDto = userSessionHolder.payload();
                 if (LoginType.USER == type && UserType.USER.getAuth() == userViewResponseDto.getAuth()) { // 로그인 사용자만 접근가능
                    return true;
                } else if (LoginType.ADMIN == type && UserType.ADMIN.getAuth() == userViewResponseDto.getAuth()) { // 관리자 사용자만 접근가능
                    return true;
                } else {
                     return true;
                 }
            }
            log.info("권한이 없어서 튕겨짐...");
            response.sendError(HttpStatus.SC_UNAUTHORIZED, "접근권한이 없습니다.");
            return false;
        }

        return true;
    }
}
