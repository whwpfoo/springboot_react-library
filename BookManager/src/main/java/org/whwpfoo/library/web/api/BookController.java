package org.whwpfoo.library.web.api;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.code.LoginType;
import org.whwpfoo.library.domain.PageRequest;
import org.whwpfoo.library.service.BookService;
import org.whwpfoo.library.web.dto.books.BookSaveRequestDto;
import org.whwpfoo.library.web.dto.books.BookViewResponseDto;
import org.whwpfoo.library.web.mvc.NeedLogin;

@RestController
@RequiredArgsConstructor
public class BookController {
    private final Logger log = LoggerFactory.getLogger(BookController.class);
    private final BookService bookService;

    @PostMapping(value = "/api/books", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.ADMIN)
    public ResponseEntity<Long> createBook(@RequestBody BookSaveRequestDto saveDto) {
        return new ResponseEntity<>(bookService.saveBook(saveDto), HttpStatus.CREATED);
    }

    @PostMapping(value = "/api/books-upload", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.ADMIN)
    public ResponseEntity<String> createBookByCsvFile(@RequestParam(name = "file", required = true) MultipartFile csvFile) {
        return new ResponseEntity<>(bookService.saveBookByCsvFile(csvFile), HttpStatus.OK);
    }

    @GetMapping(value = "/api/books", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Page<BookViewResponseDto>> listBook(@RequestParam(name = "search", required = false) String title,
                                                              @RequestParam(name = "status", required = false) BookStatus status,
                                                              final PageRequest pageable) {
        return new ResponseEntity<>(bookService.findBookList(title, status, pageable.of()), HttpStatus.OK);
    }

    @GetMapping(value = "/api/books/{book_id}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<BookViewResponseDto> readBook(@PathVariable("book_id") Long bookId) {
        return new ResponseEntity<>(bookService.findBook(bookId), HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/books", produces = "application/json; charset=UTF-8")
    @NeedLogin(LoginType.ADMIN)
    public ResponseEntity<String> deleteBook(@RequestParam(value = "book_id", required = true) Long bookId) {
        bookService.deleteBook(bookId);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
}