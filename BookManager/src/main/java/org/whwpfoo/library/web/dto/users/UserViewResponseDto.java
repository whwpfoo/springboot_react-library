package org.whwpfoo.library.web.dto.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.whwpfoo.library.domain.users.Users;

@NoArgsConstructor
@Getter
@ToString
public class UserViewResponseDto {

    private Long id;
    private String email;
    private String username;
    private int auth;

    public UserViewResponseDto(Users users) {
        this.id = users.getId();
        this.email = users.getEmail();
        this.username = users.getUsername();
        this.auth = users.getUserType().getAuth();
    }

}
