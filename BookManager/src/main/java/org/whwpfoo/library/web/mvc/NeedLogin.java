package org.whwpfoo.library.web.mvc;

import org.whwpfoo.library.code.LoginType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NeedLogin {
    LoginType value() default LoginType.ALL;
}
