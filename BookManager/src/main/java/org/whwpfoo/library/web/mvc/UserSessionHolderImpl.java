package org.whwpfoo.library.web.mvc;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;

import java.io.Serializable;

@Component
@Scope(scopeName = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class UserSessionHolderImpl implements UserSessionHolder, Serializable {

    private UserViewResponseDto userViewResponseDto;

    @Override
    public boolean isLogin() {
        return userViewResponseDto != null;
    }

    @Override
    public void login(UserViewResponseDto usersViewResponseDto) {
        this.userViewResponseDto = usersViewResponseDto;
    }

    @Override
    public UserViewResponseDto logout() {
        UserViewResponseDto ret = this.userViewResponseDto;
        this.userViewResponseDto = null;
        return ret;
    }

    @Override
    public UserViewResponseDto payload() {
        return this.userViewResponseDto;
    }

}