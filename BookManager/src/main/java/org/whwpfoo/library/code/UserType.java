package org.whwpfoo.library.code;

import lombok.Getter;

@Getter
public enum UserType {

    USER(0),
    ADMIN(1);

    private int auth;

    UserType(int type) {
        this.auth = type;
    }

}
