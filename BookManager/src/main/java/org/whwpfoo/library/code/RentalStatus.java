package org.whwpfoo.library.code;

public enum RentalStatus {
    WAITING, REJECT, ACCEPT, RETURN
}
