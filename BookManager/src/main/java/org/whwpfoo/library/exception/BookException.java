package org.whwpfoo.library.exception;

public class BookException extends CommonException {
    public static final int NOT_FOUND_BOOK = 101;
    public static final int INCORRECT_ISBN_CODE = 102;
    public static final int ALREADY_RENTAL_BOOK = 103;

    private int code;
    private String message;

    public BookException(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        switch (code) {
            case NOT_FOUND_BOOK:
                message = "도서 정보가 존재하지 않습니다";
                break;
            case INCORRECT_ISBN_CODE:
                message = "입력형식이 올바르지 않습니다";
                break;
            case ALREADY_RENTAL_BOOK:
                message = "이미 대여/신청 중인 도서입니다";
                break;
        }
        return message;
    }
}

