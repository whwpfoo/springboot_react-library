package org.whwpfoo.library.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CommonException extends RuntimeException {
    public static final int CHECKED_ERROR = 000;
    public static final int WRONG_FILE_TYPE = 001;

    @Getter private int code;
    private String message;

    public CommonException(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        switch (code) {
            case CHECKED_ERROR:
                this.message = "실행 중 오류가 발생했습니다";
                break;
            case WRONG_FILE_TYPE:
                message = ".csv 확장자의 파일 형식만 지원합니다";
                break;
        }
        return this.message;
    }
}
