package org.whwpfoo.library.exception;

public class RentalException extends CommonException {
    public static final int NOT_FOUND_RENTAL_INFO = 301;

    private int code;
    private String message;

    public RentalException(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        switch (code) {
            case NOT_FOUND_RENTAL_INFO:
                message = "대여 정보가 존재하지 않습니다";
                break;
        }
        return message;
    }
}
