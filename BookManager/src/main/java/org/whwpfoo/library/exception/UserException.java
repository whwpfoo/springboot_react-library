package org.whwpfoo.library.exception;

public class UserException extends CommonException {
    public static final int EXIST_EMAIL = 201;
    public static final int NOT_FOUND_USER = 202;
    public static final int WRONG_EMAIL_OR_PASSWORD = 203;
    public static final int NOT_MATCH_PASSWORD = 204;

    private int code;
    private String message;

    public UserException(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        switch (code) {
            case EXIST_EMAIL:
                message = "이미 존재하는 이메일 입니다";
                break;
            case NOT_FOUND_USER:
                message = "사용자 정보 조회 실패하였습니다. 다시 시도해주세요";
                break;
            case WRONG_EMAIL_OR_PASSWORD:
                message = "아이디 또는 비밀번호가 일치하지 않습니다";
                break;
            case NOT_MATCH_PASSWORD:
                message = "비밀번호가 일치하지 않습니다";
                break;
        }
        return this.message;
    }
}
