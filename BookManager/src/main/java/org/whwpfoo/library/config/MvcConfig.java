package org.whwpfoo.library.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Sort;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.util.BCryptImpl;
import org.whwpfoo.library.util.EncryptHelper;
import org.whwpfoo.library.web.mvc.LoginCheckInterceptor;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:static/")
                .setCacheControl(CacheControl.maxAge(10, TimeUnit.MINUTES));
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginCheckInterceptor())
                .addPathPatterns("/", "/**")
                .excludePathPatterns(
                        "/static", "/static/**"
                );
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new Converter<String, BookStatus>() {
            @Override
            public BookStatus convert(String bookStatus) {
                try {
                    return BookStatus.valueOf(bookStatus.toUpperCase());
                } catch (IllegalArgumentException e) {
                    return null;
                }
            }
        });
        registry.addConverter(new Converter<String, Sort.Direction>() {
            @Override
            public Sort.Direction convert(String sort) {
                try {
                    return Sort.Direction.valueOf(sort.toUpperCase());
                } catch (IllegalArgumentException e) {
                    return Sort.Direction.DESC;
                }
            }
        });
        registry.addConverter(new Converter<String, RentalStatus>() {
            @Override
            public RentalStatus convert(String rentalStatus) {
                try {
                    return RentalStatus.valueOf(rentalStatus.toUpperCase());
                } catch (IllegalArgumentException e) {
                    return null;
                }
            }
        });
    }

    @Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setSuffix(".html");
        resolver.setContentType("text/html; charset=UTF-8");
        return resolver;
    }

    @Bean
    public LoginCheckInterceptor loginCheckInterceptor() {
        return new LoginCheckInterceptor();
    }

    @Bean
    public EncryptHelper encryptConfigure() {
        return new BCryptImpl();
    }
}
