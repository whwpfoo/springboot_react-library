package org.whwpfoo.library.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application-api.yml")
@NoArgsConstructor
@Getter
@Setter
public class NaverApiConfig {

    @Value("${id}")
    public String id;

    @Value("${secret}")
    public String secret;

    @Value("${url}")
    public String url;

}
