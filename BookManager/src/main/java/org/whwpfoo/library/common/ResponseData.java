package org.whwpfoo.library.common;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseData<T> {

    private String message;
    private T data;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
