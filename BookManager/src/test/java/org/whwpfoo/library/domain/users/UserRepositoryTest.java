package org.whwpfoo.library.domain.users;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.whwpfoo.library.code.UserType;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void 회원_저장() throws Exception {
        //given
        Users user = Users.builder()
                .email("whwpfoo@gmail.com")
                .username("조준제")
                .password("123456")
                .userType(UserType.USER)
                .build();

        //when
        Users saveUser = userRepository.save(user);

        //then
        Assertions.assertThat(saveUser.getId()).isEqualTo(user.getId());
        Assertions.assertThat(saveUser.getEmail()).isEqualTo(user.getEmail());
        Assertions.assertThat(saveUser.getUsername()).isEqualTo(user.getUsername());
    }
}