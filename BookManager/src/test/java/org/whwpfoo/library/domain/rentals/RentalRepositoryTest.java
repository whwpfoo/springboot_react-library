package org.whwpfoo.library.domain.rentals;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.domain.PageRequest;
import org.whwpfoo.library.domain.books.BookRepository;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.domain.users.UserRepository;
import org.whwpfoo.library.domain.users.Users;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RentalRepositoryTest {

    @Autowired
    RentalRepository rentalRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookRepository bookRepository;



    @Before
    public void setup() {
        Users whwpfoo = Users.builder()
                .email("whwpfoo@gmail.com")
                .username("whwpfoo")
                .userType(UserType.ADMIN)
                .password("1234")
                .build();
        Long userid = userRepository.save(whwpfoo).getId();

        List<Long> bookidList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Books book = Books.builder()
                    .title("test" + i)
                    .author("test" + i)
                    .bookStatus(BookStatus.ON)
                    .build();
            Long bookid = bookRepository.save(book).getId();
            bookidList.add(bookid);
        }

        for (long bookid : bookidList) {
            rentalRepository.save(
                    Rentals.builder()
                    .users(Users.builder().id(userid).build())
                    .books(Books.builder().id(bookid).build())
                    .rentalStatus((bookid > 50) ? RentalStatus.ACCEPT : RentalStatus.WAITING)
                    .message("대여 중")
                    .build()
            );
        }

    }
    
    @Test
    public void 관리자_대여조회테스트() throws Exception {
        //given
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(0);
        pageRequest.setSize(100);
        pageRequest.setSort(Sort.Direction.DESC);

        //when
        Page<Rentals> result = rentalRepository.findByRentalListForAdmin(
                RentalStatus.WAITING,
                "whwpfoo",
                pageRequest.of()
        );

        //then
        for (Rentals r : result.getContent()) {
            System.out.println("rental id: " + r.getId() + " book title: " + r.getBooks().getTitle() + " user name: " + r.getUsers().getUsername());
        }
    }
}