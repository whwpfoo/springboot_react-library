package org.whwpfoo.library.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.whwpfoo.library.exception.BookException;
import org.whwpfoo.library.web.dto.books.BookSaveRequestDto;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NaverApiServiceTest {

    @Autowired
    private NaverApiService naverApiService;
    
    @Test
    public void 네이버_도서_ISBN_검색() throws Exception {
        //given
        String isbn = "8931447892";
        String bookInfoByJsonFormat = naverApiService.searchBookByIsbnCode(isbn);

        //when
        BookSaveRequestDto dto = BookSaveRequestDto.fromJson(bookInfoByJsonFormat);

        //then
        Assertions.assertThat(dto.getTitle()).isEqualTo("그림으로 배우는 HTTP & Network Basic (재미있게 배워보는 웹과 네트워크 입문)");
    }

    @Test(expected = BookException.class)
    public void ISBN_오류_검색() throws Exception {
        //given
        String isbn = "abcd";
        String bookInfoByJsonFormat = naverApiService.searchBookByIsbnCode(isbn);

        //when
        BookSaveRequestDto saveRequestDto = BookSaveRequestDto.fromJson(bookInfoByJsonFormat);

        //then
        fail("입력형식 오류");
    }

}