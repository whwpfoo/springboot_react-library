package org.whwpfoo.library.service;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.domain.books.BookRepository;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.exception.BookException;
import org.whwpfoo.library.web.dto.books.BookSaveRequestDto;
import org.whwpfoo.library.web.dto.books.BookViewResponseDto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import static java.lang.Thread.sleep;
import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BookServiceTest {

    @Autowired
    BookService bookService;

    @Autowired
    BookRepository bookRepository;

    @After
    public void 초기화() {
        bookRepository.deleteAll();
    }

    @Test
    public void 책_등록() {
        //given
        String isbnCode = "8931447892";
        BookSaveRequestDto saveRequestDto
                = BookSaveRequestDto
                    .builder().isbn(isbnCode)
                    .build();

        //when
        long saveBookId = bookService.saveBook(saveRequestDto);
        Books findBooks = bookRepository.findById(saveBookId).get();

        //then
        Assertions.assertThat(findBooks.getId()).isEqualTo(saveBookId);
        Assertions.assertThat(findBooks.getIsbn()).isEqualTo(isbnCode);
    }

    @Test(expected = BookException.class)
    public void 책_등록_빈값() throws Exception {
        //given
        String isbn = "ㅇㄹㅇㄴㄹ";
        BookSaveRequestDto build = BookSaveRequestDto.builder().isbn(isbn).build();

        //when
        long saveBookId = bookService.saveBook(build);
        Books entity = bookRepository.findById(saveBookId).get();

        //then
        fail("잘못된 isbn 코드");
    }

    @Test
    public void 책_등록_csv파일() {
        //given
        String result = null;
        try (BufferedReader fr = new BufferedReader(new FileReader("d:\\isbn.csv"))) {
            String line = null;
            while ((line = fr.readLine()) != null) {
                String[] isbnArray = line.split(",");
                result = bookService.saveBookByIsbnCodes(isbnArray);
            }
        } catch (Exception e) {
        }

        //then
        Assertions.assertThat(result).isNotEmpty();
        System.out.println(result);
    }

    @Test
    public void 책_조회_전체() {
        //given
        int count = 20;
        for (int i = 0; i < count; i++) {
            bookService.saveBook(BookSaveRequestDto.builder().isbn(i + "").build());
        }

        //when
        List<Books> bookListAll = bookRepository.findAll();

        //then
        Assertions.assertThat(bookListAll.size()).isEqualTo(count);
    }

    @Test
    public void 책_조회_제목으로() throws Exception {
        //given
        String isbnCode = "8931447892";
        BookSaveRequestDto saveRequestDto
                = BookSaveRequestDto
                .builder().isbn(isbnCode)
                .build();
        bookService.saveBook(saveRequestDto);

        //when
        String title = "그림으로 배우는";
        Page<BookViewResponseDto> findBookByTitle = bookService.findBookList(title, null, null);

        //then
        Assertions.assertThat(findBookByTitle.get().findFirst().get().getTitle()).containsIgnoringCase(title);
    }

    @Test
    public void 책_조회_상태값으로() throws InterruptedException {
        //given
        int count = 20;
        for (int i = 0; i < count; i++) { // default status -> BookStatus.ON
            bookService.saveBook(BookSaveRequestDto.builder().isbn(i + "").build());
        }

        //when
        Page<Books> bookListAll = bookRepository.findByBookStatus(BookStatus.ON, null);

        //then
        Assertions.assertThat(bookListAll.getTotalElements()).isEqualTo(20);
    }

    @Test
    public void 책_삭제() throws Exception {
        //given
        String isbnCode = "8931447892";
        BookSaveRequestDto saveRequestDto
                = BookSaveRequestDto
                .builder().isbn(isbnCode)
                .build();

        //when
        long saveBookId = bookService.saveBook(saveRequestDto);
        bookService.deleteBook(saveBookId);
        Books findBookEntity = bookRepository.findById(saveBookId).orElseGet(() -> null);

        //then
        Assertions.assertThat(findBookEntity).isNull();
    }
}