package org.whwpfoo.library.service;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.domain.books.BookRepository;
import org.whwpfoo.library.domain.rentals.RentalRepository;
import org.whwpfoo.library.domain.rentals.Rentals;
import org.whwpfoo.library.domain.users.UserRepository;
import org.whwpfoo.library.exception.BookException;
import org.whwpfoo.library.web.dto.books.BookSaveRequestDto;
import org.whwpfoo.library.web.dto.books.BookViewResponseDto;
import org.whwpfoo.library.web.dto.rentals.RentalResponseDto;
import org.whwpfoo.library.web.dto.users.UserSaveRequestDto;


import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RentalServiceTest {

    @Autowired
    RentalService rentalService;

    @Autowired
    RentalRepository rentalRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BookService bookService;

    @Autowired
    UserService userService;

    private long userId;
    private long[] bookIdArray;

    @Before
    public void setup() {
        long user_id = userService.saveUser(
                UserSaveRequestDto.builder()
                        .email("whwpfoo@gmail.com")
                        .password("1234")
                        .username("whwpfoo")
                        .build()
        );

        userId = user_id;
        int count = 10;
        bookIdArray = new long[count];

        for (int i = 0; i < count; i++) {
            long book_id = bookService.saveBook(
                BookSaveRequestDto.builder()
                    .isbn(i + "")
                    .build()
            );

            bookIdArray[i] = book_id;
        }
    }

    @After
    public void 초기화() {
        rentalRepository.deleteAll();
        userRepository.deleteAll();
        bookRepository.deleteAll();
    }

    @Test
    public void 도서_대여신청() throws Exception {
        //given
        long rentalRequestId = rentalService.requestRentalBook(userId, bookIdArray[0]);

        //when
        Rentals findRental = rentalRepository.findById(rentalRequestId).get();
        BookViewResponseDto findBook = bookService.findBook(findRental.getBooks().getId());

        //then
        Assertions.assertThat(findRental.getRentalStatus()).isEqualTo(RentalStatus.WAITING);
        Assertions.assertThat(findBook.getBookStatus()).isEqualTo(BookStatus.OFF);
        Assertions.assertThat(findBook.getId()).isEqualTo(bookIdArray[0]);
    }

    @Test(expected = BookException.class)
    public void 도서_대여신청_중복() throws Exception {
        //given
        long rentalRequestId = rentalService.requestRentalBook(userId, bookIdArray[0]);
        long rentalRequestId2 = rentalService.requestRentalBook(userId, bookIdArray[0]);

        //when
        fail("중복 도서 신청");

        //then
    }

    @Test
    public void 도서_대여_승인() throws Exception {
        //given

        //when
        long rentalRequestId = rentalService.requestRentalBook(userId, bookIdArray[0]);
        RentalResponseDto rentalResponseDto = rentalService.performRentalBook(rentalRequestId, RentalStatus.ACCEPT);

        //then
        Assertions.assertThat(rentalResponseDto.getRentalStatus()).isEqualTo(RentalStatus.ACCEPT);
        Assertions.assertThat(rentalResponseDto.getId()).isEqualTo(rentalRequestId);
        Assertions.assertThat(rentalResponseDto.getMessage()).isEqualTo("대여 중");
    }

    @Test
    public void 도서_대여_거절() throws Exception {
        //given

        //when
        long rentalRequestId = rentalService.requestRentalBook(userId, bookIdArray[0]);
        RentalResponseDto rentalResponseDto = rentalService.performRentalBook(rentalRequestId, RentalStatus.REJECT);
        BookViewResponseDto book = bookService.findBook(bookIdArray[0]);

        //then
        Assertions.assertThat(rentalResponseDto.getRentalStatus()).isEqualTo(RentalStatus.REJECT);
        Assertions.assertThat(rentalResponseDto.getId()).isEqualTo(rentalRequestId);
        Assertions.assertThat(rentalResponseDto.getMessage()).isEqualTo("대여 거절");
        Assertions.assertThat(book.getBookStatus()).isEqualTo(BookStatus.ON);
    }

    @Test
    public void 도서_대여_반납() throws Exception {
        //given

        //when
        long rentalRequestId = rentalService.requestRentalBook(userId, bookIdArray[0]);
        rentalService.performRentalBook(rentalRequestId, RentalStatus.ACCEPT);
        RentalResponseDto rentalResponseDto = rentalService.performRentalBook(rentalRequestId, RentalStatus.RETURN);
        BookViewResponseDto book = bookService.findBook(bookIdArray[0]);

        //then
        Assertions.assertThat(rentalResponseDto.getRentalStatus()).isEqualTo(RentalStatus.RETURN);
        Assertions.assertThat(rentalResponseDto.getId()).isEqualTo(rentalRequestId);
        Assertions.assertThat(rentalResponseDto.getMessage()).isEqualTo("도서 반납");
        Assertions.assertThat(book.getBookStatus()).isEqualTo(BookStatus.ON);
        Assertions.assertThat(rentalResponseDto.getReturnDate()).isNotNull();
    }
}