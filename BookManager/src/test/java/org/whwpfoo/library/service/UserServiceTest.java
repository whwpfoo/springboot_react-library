package org.whwpfoo.library.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.whwpfoo.library.domain.users.UserRepository;
import org.whwpfoo.library.domain.users.Users;
import org.whwpfoo.library.exception.UserException;
import org.whwpfoo.library.util.EncryptHelper;
import org.whwpfoo.library.web.dto.users.UserSaveRequestDto;
import org.whwpfoo.library.web.dto.users.UserUpdateRequestDto;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EncryptHelper encryptHelper;

    @Test
    public void 회원가입() throws Exception {
        //given
        UserSaveRequestDto user = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("1234")
                .build();

        //when
        long saveUserId = userService.saveUser(user);
        Users findUser = userRepository.findById(saveUserId).get();

        //then
        Assertions.assertThat(findUser.getEmail()).isEqualTo(user.getEmail());
        Assertions.assertThat(findUser.getUsername()).isEqualTo(user.getUsername());
        Assertions.assertThat(encryptHelper.isMatch("1234", findUser.getPassword())).isTrue();
    }

    @Test(expected = UserException.class)
    public void 회원가입_중복() throws Exception {
        //given
        UserSaveRequestDto user = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("1234")
                .build();

        UserSaveRequestDto user2 = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("1234")
                .build();

        //when
        userService.saveUser(user);
        userService.saveUser(user2);

        //then
        fail("중복회원 오류가 나야함");
    }

    @Test
    public void 회원_정보수정() throws Exception {
        //given
        UserSaveRequestDto user = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("1234")
                .build();

        //when
        long userId = userService.saveUser(user);

        UserUpdateRequestDto updateDto = UserUpdateRequestDto.builder().id(userId).username("whwp0913").build();
        Users findUser = userRepository.findById(userId).get();
        UserViewResponseDto viewDto = userService.updateUserInfo(updateDto);

        //then
        Assertions.assertThat(findUser.getUsername()).isEqualTo(updateDto.getUsername());
        Assertions.assertThat(viewDto.getId()).isEqualTo(findUser.getId());
        Assertions.assertThat(viewDto.getUsername()).isEqualTo(findUser.getUsername());
        Assertions.assertThat(viewDto.getEmail()).isEqualTo(findUser.getEmail());
    }

    @Test
    public void 회원_정보수정_비밀번호() throws Exception {
        //given
        UserSaveRequestDto user = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("dkssudgktpdy")
                .build();

        //when
        long userId = userService.saveUser(user);
        userService.changePassword(UserUpdateRequestDto.builder().id(userId).oldPassword("dkssudgktpdy").password("123456").build());
        Users findUser = userRepository.findById(userId).get();

        //then
        Assertions.assertThat(encryptHelper.isMatch("123456", findUser.getPassword())).isTrue();
    }

    @Test(expected = UserException.class)
    public void 회원_정보수정_비밀번호_틀림() throws Exception {
        //given
        UserSaveRequestDto user = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("dkssudgktpdy")
                .build();

        //when
        long userId = userService.saveUser(user);
        userService.changePassword(UserUpdateRequestDto.builder().id(userId).oldPassword("11").password("123456").build());
        Users findUser = userRepository.findById(userId).get();

        //then
        fail("기존 패스워드 입력이 틀림");
    }

    @Test
    public void 회원_로그인() throws Exception {
        //given
        UserSaveRequestDto user = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("dkssudgktpdy")
                .build();

        //when
        userService.saveUser(user);
        UserViewResponseDto viewDto = userService.login("whwpfoo@naver.com", "dkssudgktpdy");

        //then
        Assertions.assertThat(viewDto).isNotNull();
    }

    @Test(expected = UserException.class)
    public void 회원_로그인_실패() throws Exception {
        //given
        UserSaveRequestDto user = UserSaveRequestDto.builder()
                .email("whwpfoo@naver.com")
                .username("whwpfoo")
                .password("dkssudgktpdy")
                .build();

        //when
        userService.saveUser(user);
        userService.login("whwpfoo@naver.com", "dkssudgktpdy");

        //then
        fail("아이디 비밀번호가 틀렸음");
    }
}