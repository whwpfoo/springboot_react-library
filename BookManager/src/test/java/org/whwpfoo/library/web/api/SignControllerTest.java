package org.whwpfoo.library.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.whwpfoo.library.domain.users.UserRepository;
import org.whwpfoo.library.service.UserService;
import org.whwpfoo.library.web.dto.users.SignRequestDto;
import org.whwpfoo.library.web.dto.users.UserSaveRequestDto;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;
import org.whwpfoo.library.web.mvc.UserSessionHolderImpl;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SignControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @After
    public void 초기화() {
        userRepository.deleteAll();
    }
    @Test
    public void 회원가입() throws Exception {
        //given
        UserSaveRequestDto saveDto = UserSaveRequestDto.builder()
                .email("whwpfoo@gmail.com")
                .username("조준제")
                .password("1234")
                .build();

        //when
        ResultActions actions = mvc.perform(post("/users/join")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(saveDto)));

        //then
        actions
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn()
                .getResponse().getContentAsString().equalsIgnoreCase("1");
    }

    @Test
    public void 로그인() throws Exception {
        //given
        UserSaveRequestDto loginDto = UserSaveRequestDto.builder()
                .email("whwpfoo@gmail.com")
                .username("조준제")
                .password("123456").build();
        userService.saveUser(loginDto);

        SignRequestDto signDto = SignRequestDto.builder()
                .email(loginDto.getEmail())
                .password("123456")
                .build();

        //when
        ResultActions actions = mvc.perform(post("/users/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(signDto)));

        //then
        MvcResult mvcResult = actions
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void 로그아웃() throws Exception {
        //given
        UserSaveRequestDto loginDto = UserSaveRequestDto.builder()
                .email("whwpfoo@gmail.com")
                .username("조준제")
                .password("123456").build();

        userService.saveUser(loginDto);

        SignRequestDto signDto = SignRequestDto.builder()
                .email(loginDto.getEmail())
                .password("123456")
                .build();

        //when
        mvc.perform(post("/users/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(signDto)));

        UserSessionHolderImpl session = new UserSessionHolderImpl();
        session.login(new UserViewResponseDto());
        ResultActions actions = mvc.perform(get("/users/logout")
        .sessionAttr("scopedTarget.userSessionHolderImpl", session));

        //then
        actions
                .andDo(print())
                .andExpect(status().isOk());
    }
}