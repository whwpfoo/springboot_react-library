package org.whwpfoo.library.web.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.whwpfoo.library.web.dto.rentals.RentalListResponseDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TestDto {
    List<RentalListResponseDto> content;
}
