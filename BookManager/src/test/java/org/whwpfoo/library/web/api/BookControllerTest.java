package org.whwpfoo.library.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.domain.books.BookRepository;
import org.whwpfoo.library.domain.users.Users;
import org.whwpfoo.library.service.BookService;
import org.whwpfoo.library.web.dto.books.BookSaveRequestDto;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;
import org.whwpfoo.library.web.mvc.UserSessionHolder;
import org.whwpfoo.library.web.mvc.UserSessionHolderImpl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BookControllerTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BookService bookService;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON_UTF8.getType(),
                                                MediaType.APPLICATION_JSON_UTF8.getSubtype());

    private Map<String, Object> sessionMap = new HashMap<>();

    @Before
    public void 사용자_세션_할당_관리자() {
        UserSessionHolder session = new UserSessionHolderImpl();
        session.login(new UserViewResponseDto(
                Users.builder().userType(UserType.ADMIN).build()));
        sessionMap.put("scopedTarget.userSessionHolderImpl", session);
    }

    @Ignore
    @Before
    public void 사용자_세션_할당_사용자() {
        UserSessionHolder session = new UserSessionHolderImpl();
        session.login(new UserViewResponseDto(
                Users.builder().userType(UserType.USER).build()));
        sessionMap.put("scopedTarget.userSessionHolderImpl", session);
    }

    @After
    public void 초기화() {
        bookRepository.deleteAll();
    }

    @Test
    public void 도서_등록() throws Exception {
        //given
        BookSaveRequestDto saveDto = new BookSaveRequestDto();
        saveDto.setIsbn("1023");

        //when
        final ResultActions actions = mvc.perform(post("/books")
                    .sessionAttrs(sessionMap)
                    .contentType(contentType)
                    .content(objectMapper.writeValueAsString(saveDto)));

        //then
        actions
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
    }
    
    @Test
    public void 도서_등록_csv파일() throws Exception {
        //given
        FileInputStream fis = new FileInputStream("D:\\isbn.csv");
        MockMultipartFile isbnCodeFile = new MockMultipartFile
                ("file","isbn.csv" , "multipart/form-data", fis);

        //when
        ResultActions actions = mvc.perform(multipart("/books-upload")
                .file(isbnCodeFile)
                .sessionAttrs(sessionMap));

        //then
        MvcResult mvcResult = actions
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void 도서_조회() throws Exception {
        //given
        BookSaveRequestDto saveDto = new BookSaveRequestDto();
        saveDto.setIsbn("1023");

        //when
        String bookId = mvc.perform(post("/books")
                .sessionAttrs(sessionMap)
                .contentType(contentType)
                .content(objectMapper.writeValueAsString(saveDto)))
                .andReturn().getResponse().getContentAsString();

        ResultActions actions = mvc.perform(get("/books/" + bookId));

        //then
        actions
                .andDo(print())
                .andExpect(status().isOk());
    }
    
    @Test
    public void 도서_리스트조회_일반() throws Exception {
        //given
        FileInputStream fis = new FileInputStream("D:\\isbn.csv");
        MockMultipartFile isbnCodeFile = new MockMultipartFile
                ("file","isbn.csv" , "multipart/form-data", fis);
        mvc.perform(multipart("/books-upload").file(isbnCodeFile).sessionAttrs(sessionMap));

        //when
        ResultActions actions = mvc.perform(get("/books")
                .sessionAttrs(sessionMap)
                .contentType(contentType)
                .param("page", "1")
                .param("size", "100")
                .param("sort", "DESC"));

        //then
        actions
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void 도서_리스트조회_제목() throws Exception {
        //given
        String result = null;
        try (BufferedReader fr = new BufferedReader(new FileReader("d:\\isbn.csv"))) {
            String line = null;
            while ((line = fr.readLine()) != null) {
                String[] isbnArray = line.split(",");
                result = bookService.saveBookByIsbnCodes(isbnArray);
            }
        } catch (Exception e) {
        }

        //when
        ResultActions actions = mvc.perform(get("/books")
                .sessionAttrs(sessionMap)
                .contentType(contentType)
                .param("page", "1")
                .param("size", "10")
                .param("sort", "DESC")
                .param("title", "Java의")
                .param("status", "OFF"));

        //then
        actions
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void 도서_삭제() throws Exception {
        //given
        String result = null;
        try (BufferedReader fr = new BufferedReader(new FileReader("d:\\isbn.csv"))) {
            String line = null;
            while ((line = fr.readLine()) != null) {
                String[] isbnArray = line.split(",");
                result = bookService.saveBookByIsbnCodes(isbnArray);
            }
        } catch (Exception e) {
        }

        //when
        ResultActions actions = mvc.perform(delete("/books/" + "1")
                .sessionAttrs(sessionMap)
                .contentType(contentType));

        //then
        actions
                .andDo(print())
                .andExpect(content().string("success"));
    }
}