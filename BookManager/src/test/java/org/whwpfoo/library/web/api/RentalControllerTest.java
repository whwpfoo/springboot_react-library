package org.whwpfoo.library.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.whwpfoo.library.code.BookStatus;
import org.whwpfoo.library.code.RentalStatus;
import org.whwpfoo.library.code.UserType;
import org.whwpfoo.library.domain.books.BookRepository;
import org.whwpfoo.library.domain.books.Books;
import org.whwpfoo.library.domain.rentals.RentalRepository;
import org.whwpfoo.library.domain.users.UserRepository;
import org.whwpfoo.library.domain.users.Users;
import org.whwpfoo.library.web.dto.rentals.RentalListResponseDto;
import org.whwpfoo.library.web.dto.users.UserViewResponseDto;
import org.whwpfoo.library.web.mvc.UserSessionHolder;
import org.whwpfoo.library.web.mvc.UserSessionHolderImpl;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RentalControllerTest {

    final Logger log = LoggerFactory.getLogger(RentalControllerTest.class);

    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    RentalRepository rentalRepository;

    Users user1;
    Users admin;

    UserSessionHolder userSession;
    UserSessionHolder adminSession;

    @Before
    public void setup() {
        /////////////////////////////////////////// 회원추가
        user1 = userRepository.save(Users.builder()
                .email("whwpfoo@gmail.com")
                .username("whwpfoo")
                .userType(UserType.USER)
                .password("1234").build());
        admin = userRepository.save(Users.builder()
                .email("admin@gmail.com")
                .username("admin")
                .userType(UserType.ADMIN)
                .password("1234").build());
        /////////////////////////////////////////// 회원세션 추가
        userSession = new UserSessionHolderImpl();
        userSession.login(new UserViewResponseDto(user1));
        adminSession = new UserSessionHolderImpl();
        adminSession.login(new UserViewResponseDto(admin));
        /////////////////////////////////////////// 도서 추가
        for (int i = 0; i < 100; i++) {
            bookRepository.save(Books.builder()
                    .title("" + i)
                    .author("" + i)
                    .bookStatus(BookStatus.ON)
                    .build());
        }
    }

    @After
    public void 초기화() {

        rentalRepository.deleteAll();
        bookRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void 대여_신청() throws Exception {
        //given

        //when
        대여신청_메소드(null);
    }

    private void 대여신청_메소드(String bookId) throws Exception {
        ResultActions actions = mvc.perform(post("/rentals/book-request")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .sessionAttr("scopedTarget.userSessionHolderImpl", userSession)
                .param("bookId", (bookId == null ? "1" : bookId)));

        //then
        MvcResult mvcResult = actions
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void 대여_확인() throws Exception {
        //given
        대여확인_메소드(null, null);
        return;

    }

    private void 대여확인_메소드(String rentalId, String status) throws Exception {
        //when
        ResultActions actions = mvc.perform(post("/rentals/book-response")
                .param("rentalId", (rentalId == null ? "1" : rentalId))
                .param("status", (status == null ? "accept" : status)) // status 상태 변경하며 테스트
                .sessionAttr("scopedTarget.userSessionHolderImpl", adminSession));

        //then
        actions
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void 대여_조회_관리자() throws Exception {
        //given
        for (int i = 1; i <= 100; i++) {
            대여신청_메소드(i + "");
            if (i <= 20) 대여확인_메소드(i + "", RentalStatus.ACCEPT.toString()); // 1 ~ 20: 승인
            if (i > 20 && i <= 50) 대여확인_메소드(i + "", RentalStatus.REJECT.toString()); // 21 ~ 50 거절
            if (i > 50 && i <= 100) 대여확인_메소드(i + "", RentalStatus.RETURN.toString()); // 51 ~ 100 반납
        }

        //when
        ResultActions actions = mvc.perform(get("/rentals/books")
                .sessionAttr("scopedTarget.userSessionHolderImpl", adminSession)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("page", "1")
                .param("size", "200")
                .param("sort", "DESC")
                .param("status", "return")
                .param("search", "whwpfoo"));

        //then
        MvcResult mvcResult = actions
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String contentFromString = mvcResult.getResponse().getContentAsString();
        TestDto content = objectMapper.readValue(contentFromString, TestDto.class);
        for (RentalListResponseDto dtd : content.getContent()) {
//            log.info("r_id:{}, r_status:{}, b_title:{}, u_username:{}",
//            dtd.getId(), dtd.getRentalStatus(), dtd.getBooks().getTitle(), dtd.getUsers().getUsername());
        }
    }

    @Test
    public void 대여_조회_사용자() throws Exception {
        //given
        for (int i = 1; i <= 100; i++) {
            대여신청_메소드(i + "");
            if (i <= 20) 대여확인_메소드(i + "", RentalStatus.ACCEPT.toString()); // 1 ~ 20: 승인
            if (i > 20 && i <= 50) 대여확인_메소드(i + "", RentalStatus.REJECT.toString()); // 21 ~ 50 거절
            if (i > 50 && i <= 100) 대여확인_메소드(i + "", RentalStatus.RETURN.toString()); // 51 ~ 100 반납
        }

        //when
        ResultActions actions = mvc.perform(get("/rentals/books")
                .sessionAttr("scopedTarget.userSessionHolderImpl", userSession)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("status", "return")
                .param("page", "1")
                .param("size", "100")
                .param("sort", "DESC"));

        //then
        MvcResult mvcResult = actions
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }
}