import Vue from 'vue';
import App from './App.vue';
import router from './router/index';
import VueMoment from 'vue-moment';
import Paginate from 'vuejs-paginate';

Vue.component('paginate', Paginate); // 전역으로 페이징 컴포넌트 등록
Vue.use(VueMoment);

new Vue({
  render: h => h(App),
  router,
}).$mount("#app");
