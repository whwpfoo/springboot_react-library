import Vue from 'vue';
import VueRouter from 'vue-router';
import BookList from "../components/book/BookList";
import NotFound from "../components/NotFound";
import UserLogin from "../components/login/UserLogin";
import UserJoin from "../components/login/UserJoin";
import RentalList from '../components/rental/RentalList';
import {utils} from '../service/util.service';
import {userService} from '../service/user.service';
import BookViewItem from "../components/book/BookViewItem";
import AdminBookList from "../components/book/AdminBookList";
import AdminRentalList from "../components/rental/AdminRentalList";
import UserInfo from "../components/user/UserInfo";

Vue.use(VueRouter);
const requiredAuthPaths  = ['rental-user', 'rental-admin', 'user-info', 'admin-book-list', 'user-info'];

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: BookList },
        { path: '*', component: NotFound },
        { path: '/books', component: BookList, name: 'book-list' },
        { path: '/books/:id', component: BookViewItem, name: 'book-view' },
        { path: '/admin/books', component: AdminBookList, name: 'admin-book-list',
          beforeEnter: ((to, from, next) => {
              const {auth} = utils.getSession();
              if (auth !== 1) {
                  alert('접근권한이 없습니다.');
                  router.go(-1);
              } else {
                  next();
              }
          })
        },
        { path: '/users/login', component: UserLogin, name: 'login' },
        { path: '/users/join', component: UserJoin, name: 'join' },
        { path: '/users/:id', component: UserInfo, name: 'user-info',
          beforeEnter: ((to, from, next) => {
              const { params : { id : id } } = to;
              const { id : sessionId } = utils.getSession();
              if (id != sessionId) {
                  alert('잘못된 접근입니다.');
                  router.go(-1);
              } else {
                  next();
              }
          })
        },
        { path: '/users/:id/rentals', component: RentalList, name: 'rental-user',
          beforeEnter: ((to, from, next) => {
              const { params : { id : id } } = to;
              const { id : sessionId } = utils.getSession();
              if (id != sessionId) {
                  next(`/users/${sessionId}/rentals`);
              }
              next();
          })
        },
        { path: '/admin/rentals', component: AdminRentalList, name: 'admin-rental-list',
          beforeEnter: ((to, from, next) => {
              const {auth} = utils.getSession();
              if (auth !== 1) {
                  alert('접근권한이 없습니다.');
                  router.go(-1);
              } else {
                  next();
              }
          })
        },

    ]
});

router.beforeEach(async (to, from, next) => {
    const {name, path, params : { id } } = to;
    if (requiredAuthPaths.includes(name)) {
        const { data } = await userService.getSession();
        if (!data) {
            utils.initSession();
            alert('세션이 만료되었습니다');
            location.href = '/users/login';
            return;
        }
    }
    next();
});

export default router;