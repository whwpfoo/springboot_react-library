import axios from 'axios';
import {utils} from './util.service';

export const bookService = {
    addBook: async (isbnCode) => {
        try {
            const param = {
                isbn: isbnCode
            }
            return await axios.post('/api/books', param);
        } catch (err) {
            console.log('book find fail');
            utils.printErrorMessageAndAlert(err);
        }
    },
    addBookByUpload: async (formData) => {
        try {
            return await axios.post('/api/books-upload', formData, {
                headers: {"Content-Type": "multipart/form-data"}
            });
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    },
    getBook: async (bookId) => {
        try {
            return await axios.get(`/api/books/${bookId}`);
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    },
    getBookList: async ({page = 1, size = 10, sort = 'desc', search = null, status = null}) => {
        try {
            return await axios.get('/api/books', {
                params: {
                    page: page,
                    size: size,
                    sort: sort,
                    search: search,
                    status: status
                }
            });
        } catch (err) {
            alert(err.response.data);
            console.error(err);
        }
    },
    deleteBook: (bookId) => {
        try {
            return axios.delete('/api/books/', {
                params: {
                    book_id: bookId
                }
            });
        } catch (err) {
            alert(err.response.data);
            console.error(err);
        }
    },
    setImage: (image) => {
        return image.replace('type=m1', '');
    },
}


