import axios from 'axios';
import {utils} from './util.service';

export const userService = {
    getSession: () => {
        return axios.get('/api/users/session');
    },
    login: async ({email, password}) => {
        try {
            return await axios.post('/api/users/login', {
                email: email,
                password: password
            });
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    },
    join: async ({email, username, password}) => {
        try {
            return await axios.post('/api/users/join', {
                email: email,
                username: username,
                password: password
            });
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    },
    logout: () => {
        try {
            axios.get('/api/users/logout');
        } catch (err) {
            //
        } finally {
            utils.initSession();
            alert('로그아웃 되었습니다');
            location.href = '/users/login';
        }
    },
    checkForm: (form) => {
        for (const key in form) {
            const property = form[key];
            if (property === '' || property === null || property === undefined) {
                return false;
            }
        }
        return true;
    },
    updatePassword: async ({id, newPassword, oldPassword}) => {
        try {
            return await axios.put('/api/users/update', {
                id: id,
                oldPassword: oldPassword,
                password: newPassword

            });
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    }
}