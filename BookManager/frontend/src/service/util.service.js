export const utils = {
    printErrorMessageAndAlert: (err = undefined) => {
        alert(err.response.data);
    },
    setSession: (name = 'userSession', data) => {
        sessionStorage.setItem(name, JSON.stringify(data));
    },
    getSession: (name = 'userSession') => {
        return JSON.parse(sessionStorage.getItem(name));
    },
    initSession: () => {
        sessionStorage.clear();
    }
}