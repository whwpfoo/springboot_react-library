import axios from 'axios';
import {utils} from './util.service';

export const rentalService = {
    requestRental: async (bookId) => {
        try {
            const session = utils.getSession();
            if (session) {
                return await axios.post('/api/rentals/book-request', null,  {
                    params: {
                        book_id: bookId
                    }
                });
            } else {
                alert('권한이 없습니다. 로그인을 해주세요');
            }
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    },
    responseRental: async (rentalId, status) => {
        try {
            return await axios.post('/api/rentals/book-response', null, {
                params: {
                    rentalId: rentalId,
                    status: status
                }
            });
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    },
    getRentalList: async ({status = null, search = null, page = 1, size = 10, sort = 'desc'}) => {
        try {
            return await axios.get('/api/rentals/books', {
                params: {
                    status: status,
                    search: search,
                    page: page,
                    size: size,
                    sort: sort
                }
            })
        } catch (err) {
            utils.printErrorMessageAndAlert(err);
        }
    }
}